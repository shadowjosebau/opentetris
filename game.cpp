#include "Game.h"
#include "TetrisPieces.h"
#include <iostream>


Game::Game()
{
	for (int i = 0; i < MAX_HEIGHT; i++)
	{
		for (int j = 0; j < MAX_WIDTH; j++)
		{
			map[i][j] = 0;

		}
	}
}

void Game::DumpMap()
{
	for (int i = 0; i < MAX_HEIGHT; i++)
	{
		for (int j = 0; j < MAX_WIDTH; j++)
		{
			printf("%d", map[i][j]);

		}
		printf("\n");
	}
}