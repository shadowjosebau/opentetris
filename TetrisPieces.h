#pragma once


class TetrisPieces
{
public:
	int StraightLine[4] = { 1,1,1,1 };
	int LPiece[2][3] =
	{
		{1,0,0},
		{1,1,1}
	};

	int ReverseLPiece[2][3] =
	{
		{0,0,1},
		{1,1,1}
	};

	int square[2][2] =
	{
		{1,1},
		{1,1}
	};

	int ZPiece[2][3] =
	{
		{0,1,1},
		{1,1,0}
	};
	int ReverseZPiece[2][3] =
	{
		{1,1,0},
		{0,1,1}
	};
	int TPiece[2][3] =
	{
		{0,1,0},
		{1,1,1}
	};



};