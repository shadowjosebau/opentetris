#pragma once

#define MAX_WIDTH	10
#define MAX_HEIGHT	20


class Game
{
public:
	int map[MAX_HEIGHT][MAX_WIDTH];
	Game();

	void DumpMap();

	void TransposeMatrix();

};